
#include <SoftwareSerial.h>

// Constructor is (receive, send, invers_logical=false
// SoftwareSerial xbee(3,4);
// calling the constructor here causes a linker error.
// To get around that, the xbee is initialized in the setup scope,
// and placed here
SoftwareSerial* obd;

//This is a character buffer that will store the data from the serial port
char rxData[1024];
char rxIndex=0;

void send_recv(char* command);
void getResponse(int lines);
void o2_sensors(void);


void setup() {
    pinMode(13, OUTPUT);
	
	// This is retarded. BUT, it avoids a linker error
	// For what ever reason, linking fails with the constructor
	// in the global space
	// SoftwareSerial* initializerxbee(3,4);
	// xbee = new SoftwareSerial(3,4); // &initializerxbee;
	// XBee radio is on main serial
    // (*xbee).begin(57600);
	Serial.begin(57600);
	
	obd = new SoftwareSerial(2,3);
	
	// A baud rate PROVEN to work
	(*obd).begin(9600);
	
	//Wait for a little while before sending the reset command to the OBD-II-UART
	delay(1500);
	//Reset the OBD-II-UART
	(*obd).println("ATZ");
	delay(100);
	(*obd).flush();
	
	/*
	(*obd).println("ATSP0");
	getResponse();
	getResponse();
	Serial.println("ATSP0");
	Serial.write(rxData);
	Serial.flush();
	*/
	
	//Wait for a bit before starting to send commands after the reset.
	delay(2000);
  
	//Delete any data that may be in the serial port before we begin.
	(*obd).flush();

	// Fill the list of commands
}

void loop() {
	digitalWrite(13, !digitalRead(13));
	
	//Delete any data that may be in the serial port before we begin.
	(*obd).flush();
	
	/*
	send_recv("ATRV");
	Serial.print("Sys Volt: ");
	Serial.println(rxData);
	*/
	
	send_recv("0104");
	int engine_load = (strtol(&rxData[6],0,16) * 100) / 255;
	Serial.print("Engine Load: ");
	Serial.print(engine_load);
	Serial.println(" %");
	
	send_recv("0105");
	int coolant_temp = strtol(&rxData[6],0,16) - 40;
	Serial.print("Coolant: ");
	Serial.print(coolant_temp); 
	Serial.println(" C");

	send_recv("010B");
	int intake_pressure = strtol(&rxData[6],0,16);
	Serial.print("Intake Pressure: ");
	Serial.print(intake_pressure); 
	Serial.println(" kPa");	
	
	send_recv("010C");
	int vehicleRPM = ((strtol(&rxData[6],0,16)*256)+strtol(&rxData[9],0,16))/4;
	Serial.print("RPM: ");
	Serial.print(vehicleRPM); 
	Serial.println(" rpm");

	send_recv("010D");
	int vehicleSpeed = strtol(&rxData[6],0,16);
	Serial.print("Speed: ");
	Serial.print(vehicleSpeed);
	Serial.println(" km/h");

	
	send_recv("010F");
	int intake_temp = (strtol(&rxData[6],0,16) - 40);
	Serial.print("Intake Temp: ");
	Serial.print(intake_temp);
	Serial.println(" C");
	
	send_recv("0110");
	int maf_flow = ((strtol(&rxData[6],0,16)*256) + strtol(&rxData[9],0,16)) / 100;
	Serial.print("MAF Flow: ");
	Serial.print(maf_flow);
	Serial.println(" g/s");
	
	send_recv("0111");
	int throttle_pct = (strtol(&rxData[6],0,16)*100) / 255;
	Serial.print("Throttle: ");
	Serial.print(throttle_pct);
	Serial.println(" %");
	
	o2_sensors();

	
	send_recv("0133");
	int barrometric = strtol(&rxData[6],0,16);
	Serial.print("Barrometric Pressure: ");
	Serial.print(barrometric);
	Serial.println(" kPa");
			
	/* I don't know what relative throttle is
	send_recv("0145");
	int rel_throttle = strtol(&rxData[6],0,16)*100/255;
	Serial.print("Relative Throttle: ");
	Serial.print(rel_throttle);
	Serial.println(" %");
	*/
	
	send_recv("0146");
	int ambient_air = strtol(&rxData[6],0,16)-40;
	Serial.print("Ambient Air: ");
	Serial.print(ambient_air);
	Serial.println(" C");

	Serial.println("");
	//Give the OBD bus a rest for this period
	delay(1000);
}

// Placed here because it's nasty and I don't like it
void o2_sensors() {
	send_recv("0114");
	int b1s1v = strtol(&rxData[6],0,16)/200;
	int b1s1trim = (strtol(&rxData[9],0,16)-128) * 100/128;
	Serial.print("Bank1 S1: ");
	Serial.print(b1s1v);
	Serial.print(" V ");
	Serial.print(b1s1trim);
	Serial.println(" %");
	
	/*
	send_recv("0115");
	int b1s2v = strtol(&rxData[6],0,16)/200;
	int b1s2trim = (strtol(&rxData[9],0,16)-128) * 100/128;
	Serial.print("Bank1 S2: ");
	Serial.print(b1s2v);
	Serial.print(" V ");
	Serial.print(b1s2trim);
	Serial.println(" %");
	
	send_recv("0116");
	int b1s3v = strtol(&rxData[6],0,16)/200;
	int b1s3trim = (strtol(&rxData[9],0,16)-128) * 100/128;
	Serial.print("Bank1 S3: ");
	Serial.print(b1s3v);
	Serial.print(" V ");
	Serial.print(b1s3trim);
	Serial.println(" %");
	
	send_recv("0117");
	int b1s4v = strtol(&rxData[6],0,16)/200;
	int b1s4trim = (strtol(&rxData[9],0,16)-128) * 100/128;
	Serial.print("Bank1 S4: ");
	Serial.print(b1s4v);
	Serial.print(" V ");
	Serial.print(b1s4trim);
	Serial.println(" %");
	
	send_recv("0118");
	int b2s1v = strtol(&rxData[6],0,16)/200;
	int b2s1trim = (strtol(&rxData[9],0,16)-128) * 100/128;
	Serial.print("Bank2 S1: ");
	Serial.print(b2s1v);
	Serial.print(" V ");
	Serial.print(b2s1trim);
	Serial.println(" %");
	*/
}

void send_recv(char* command) {
	(*obd).println(command);
	int lines = 1;
	if (strcmp(command, "0100"))
		lines = 2;
	getResponse(lines);	// echo
	getResponse(lines);	// data
	
	// Serial.print("Receive: ");
	// Serial.println(rxData);
	
	// delay(10);	// give OBD a break
	(*obd).flush();	// clear out old data
}

//The getResponse function collects incoming data from the UART into the rxData buffer
// and only exits when a carriage return character is seen. Once the carriage return
// string is detected, the rxData buffer is null terminated (so we can treat it as a string)
// and the rxData index is reset to 0 so that the next string can be copied.
void getResponse(int lines){
	char inChar=0;
	//Keep reading characters until we get a carriage return
	while(inChar != '\r' && lines > 0){
		//If a character comes in on the serial port, we need to act on it.
		if((*obd).available() > 0){
			//Start by checking if we've received the end of message character ('\r').
			if((*obd).peek() == '\r'){
				lines--;
				//Clear the Serial buffer
				inChar = (*obd).read();
				// Add the newline to the buffer. Have too many characters is fine
				rxData[rxIndex++]=inChar;
			}
			// If we didn't get the end of message character, just add the new character to the string.
			else{
				//Get the new character from the Serial port.
				inChar = (*obd).read();
				//Add the new character to the string, and increment the index variable.
				rxData[rxIndex++]=inChar;
			}
		}
	}
  
	//Put the end of string character on our data string
	rxData[rxIndex]='\0';
	//Reset the buffer index so that the next character goes back at the beginning of the string.
	rxIndex=0;
}
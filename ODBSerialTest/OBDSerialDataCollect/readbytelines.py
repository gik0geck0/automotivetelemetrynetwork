#!/usr/bin/env python3

from threading import Thread, Event
from threading import Lock

import csv
import struct
import serial
import sys

if len(sys.argv) < 2:
    # print("Usage: %s <port> [baud]" % sys.argv[0])
    # exit(-1)
    dev = sys.stdin.buffer
else:
    if len(sys.argv) > 2:
        baud = int(sys.argv[2])
    else:
        baud = 57600
    dev = serial.Serial(sys.argv[1], baud)


while True:

    bstr = dev.read(1)
    if len(bstr) == 0:
        print("Breaking")
        break

    while bstr[-1] != ord('\n'):
        bstr += dev.read(1)
        print("bstr now ", bstr)
    print(bstr)
print("DONE")

dev.close()

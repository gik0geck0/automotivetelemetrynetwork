#!/usr/bin/env python3

from threading import Thread, Event
from threading import Lock

import csv
import obd
import struct
import serial
import sys

debug = False

if len(sys.argv) < 2:
    print("Usage: %s <port> [baud]" % sys.argv[0])
    exit(-1)
if len(sys.argv) > 2:
    baud = int(sys.argv[2])
else:
    baud = 57600

dev = serial.Serial(sys.argv[1], baud)

##########
#### Global PID info
##########

nbytes_by_pid = {}
pid_name = {}

with open("obd_pid_info.csv", newline='') as csvfile:
    inforeader = csv.reader(csvfile)
    next(inforeader, None)  # skip the headers
    for row in inforeader:
        pidint = int(row[0],16) + 256
        nbytes_by_pid[pidint] = int(row[1])
        # print("Bytes for pid %s: %s" % (pidint, nbytes_by_pid[pidint]))
        pid_name[pidint] = row[2]
        # print("Pid name for %s: %s" % (pidint, pid_name[pidint]))
        # 3 = min val
        # 4 = max val
        # 5 = formula

##########
#### End of global, tedious PID info
##########

# Contains the current value of each PID
current_val = {}

while True:
    pidstr = dev.read(2)
    pid = struct.unpack("H", pidstr)[0]   # uint16_t
    if pid not in nbytes_by_pid:
        bstr = dev.read()
        while bstr[-1] != ord('\n'):
            bstr += dev.read()
            # print("Collected: ", bstr[-1])
        print("Unknown PID: %s" % (pidstr + bstr))
        continue

    time = struct.unpack("I", dev.read(4))[0]  # uint32_t
    data_length = nbytes_by_pid[pid]
    data = []
    # Every single time, it's expected that all 4 bytes will be sent
    for i in range(0,4):
        data.append(struct.unpack("B", dev.read(1))[0])
        data_length -= 1

    if debug:
        for datum in data:
            print(datum, end=", ")
        print("")

    # Clear to endl
    bstr = dev.read()
    while bstr[-1] != ord('\n'):
        bstr += dev.read()
        # print("Collected: ", bstr[-1])
    if len(bstr) > 2:
        print("Extra Trailing bytes: ", bstr)
    current_val[pid] = obd.processData((pid,time,data))
    print(current_val[pid])
    # print(obd.processData((pid,time,data)))

dev.close()

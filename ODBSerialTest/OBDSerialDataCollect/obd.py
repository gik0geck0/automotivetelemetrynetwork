
import csv

##########
#### Load the Global PID Info
##########

nbytes_by_pid = {}
pid_name = {}

with open("obd_pid_info.csv", newline='') as csvfile:
    inforeader = csv.reader(csvfile)
    next(inforeader, None)  # skip the headers
    for row in inforeader:
        pidint = int(row[0],16) + 256
        nbytes_by_pid[pidint] = int(row[1])
        # print("Bytes for pid %s: %s" % (pidint, nbytes_by_pid[pidint]))
        pid_name[pidint] = row[2]
        # print("Pid name for %s: %s" % (pidint, pid_name[pidint]))
        # 3 = min val
        # 4 = max val
        # 5 = formula

# Python module that contains static PID Info and processing

# For what ever reason, some PIDs acutally contain two sets of values.
# For example, 0x0114 contains O2 sensor Voltage AND O2 sensor Trim %
#   to account for this, a single pid's data may be split into two different sets with the same PID, but different names
# processData :: (PID, Time, [Byte]) -> [(Name, PID, Time, Value)]
def processData(dataTuple):
    vals = []
    if dataTuple[0] in range(0x0114,0x011B):
        # This is O2 Sensor stuff: split into Voltage and Trim %
        vals.append((pid_name[dataTuple[0]] + " Voltage", dataTuple[0], dataTuple[1]
                , dataTuple[2][0]/200))

        vals.append((pid_name[dataTuple[0]] + " Trim %", dataTuple[0], dataTuple[1]
                , dataTuple[2][1]-128))

    # O2 WR Lambda is pretty weird
    elif dataTuple[0] in range(0x0124, 0x012B):
        # O2 WR Lambda: Equivalence ratio + Voltage
        vals.append((pid_name[dataTuple[0]] + " Equiv. Ratio", dataTuple[0], dataTuple[1]
                , (dataTuple[2][0]*256 + dataTuple[2][1]) / 32767))

        vals.append((pid_name[dataTuple[0]] + " Voltage", dataTuple[0], dataTuple[1]
                , (dataTuple[2][2]*256 + dataTuple[2][3]) / 8192))

    elif dataTuple[0] in range(0x0134, 0x013B):
        # O2 WR Lambda: Equivalence ratio + CURRENT (Amperage)
        vals.append((pid_name[dataTuple[0]] + " Equiv. Ratio", dataTuple[0], dataTuple[1]
                , (dataTuple[2][0]*256 + dataTuple[2][1]) / 32767))

        vals.append((pid_name[dataTuple[0]] + " Current", dataTuple[0], dataTuple[1]
                , (dataTuple[2][2]*256 + dataTuple[2][3]) / 256 - 128))

    # (A-128) * 100/128
    elif dataTuple[0] in (list(range(0x0106,0x0109)) + [0x012D]):
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0]*-128) * 100/128))

    # Temperature
    # A - 40
    elif dataTuple[0] in [0x0105, 0x010F, 0x0146, 0x015C]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , dataTuple[2][0] - 40))

    # A * 3. Seems to be for high-pressure params. Like fuel pressure. Actually... only fuel pressure
    elif dataTuple[0] in [0x010A]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , dataTuple[2][0] * 3))

    # A     The value IS the value. Cool. Done
    elif dataTuple[0] in [0x010B, 0x010D, 0x0130, 0x0133]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , dataTuple[2][0]))

    # (A *256 + B) / 4
    elif dataTuple[0] in [0x010C]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0]*256 + dataTuple[2][1]) / 4))

    # (A - 128) / 2
    elif dataTuple[0] in [0x010E]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] - 128) / 2))

    # (A*256) + B / 100
    elif dataTuple[0] in [0x0110]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] *256 + dataTuple[2][1]) / 100))

    # (A*256) + B
    elif dataTuple[0] in [0x0121, 0x0131, 0x014D, 0x014E]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , dataTuple[2][0] *256 + dataTuple[2][1]))

    # ((A*256) + B) * 0.079
    elif dataTuple[0] in [0x0122]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] *256 + dataTuple[2][1])* 0.079))

    # ((A*256) + B) * 0.079
    elif dataTuple[0] in [0x0123]:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] *256 + dataTuple[2][1])* 10))

    # ((A*256) + B) / 4 where A and B are signed
    elif dataTuple[0] in [0x0132]:
        if dataTuple[2][0] > 127:
            dataTuple[2][0] -= 256
        if dataTuple[2][1] > 127:
            dataTuple[2][1] -= 256

        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] * 256 + dataTuple[2][1])))

    # ((A*256)+B) / 10 - 40
    elif dataTuple[0] in range(0x013C, 0x013F):
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] *256 + dataTuple[2][1]) / 10 - 40))

    # ((A*256) + B) / 1000
    elif dataTuple[0] in range(0x013C, 0x013F):
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] *256 + dataTuple[2][1]) / 1000))

    # ((A*256) + B) / 32768
    elif dataTuple[0] in range(0x013C, 0x013F):
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , (dataTuple[2][0] *256 + dataTuple[2][1]) / 32768))

    # TODO: I got tired of writing these, and there are no more that My car supports. So I'm done

    # Final catch: make it a % across the byte-range
    # A*100/255
    else:
        vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1]
                , dataTuple[2][0] * 100/255))

    return vals


def reverse(dataTuple):
    pidtuple = (dataTuple[0][1], dataTuple[0][2], [0,0,0,0])
    if dataTuple[0][1] in range(0x0114,0x011B):
        # This is O2 Sensor stuff: split into Voltage and Trim %
        pidtuple[2][0] = int(round(dataTuple[0][3] * 200))
        pidtuple[2][1] = int(round(dataTuple[1][3] + 128))

        #vals.append((pid_name[dataTuple[0]] + " Voltage", dataTuple[0], dataTuple[1], dataTuple[2][0]/200))
        # vals.append((pid_name[dataTuple[0]] + " Trim %", dataTuple[0], dataTuple[1], dataTuple[2][1]-128))

    # O2 WR Lambda is pretty weird
    elif dataTuple[0][1] in range(0x0124, 0x012B):
        pidtuple[2][0] = int(round(dataTuple[0][3]*32767 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3]*32767 % 256))

        pidtuple[2][3] = int(round(dataTuple[1][3]*8192 // 256))
        pidtuple[2][4] = int(round(dataTuple[1][3]*8192 % 256))

        # O2 WR Lambda: Equivalence ratio + Voltage
        # vals.append((pid_name[dataTuple[0]] + " Equiv. Ratio", dataTuple[0], dataTuple[1], (dataTuple[2][0]*256 + dataTuple[2][1]) / 32767))
        #      vals.append((pid_name[dataTuple[0]] + " Voltage", dataTuple[0], dataTuple[1], (dataTuple[2][2]*256 + dataTuple[2][3]) / 8192))

    elif dataTuple[0][1] in range(0x0134, 0x013B):
        pidtuple[2][0] = int(round(dataTuple[0][3]*32767 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3]*32767 % 256))

        pidtuple[2][3] = int(round((dataTuple[1][3] + 128) * 256 // 256))
        pidtuple[2][4] = int(round((dataTuple[1][3] + 128) * 256 % 256))
        # O2 WR Lambda: Equivalence ratio + CURRENT (Amperage)
        # vals.append((pid_name[dataTuple[0]] + " Equiv. Ratio", dataTuple[0], dataTuple[1], (dataTuple[2][0]*256 + dataTuple[2][1]) / 32767))
        #      vals.append((pid_name[dataTuple[0]] + " Current", dataTuple[0], dataTuple[1], (dataTuple[2][2]*256 + dataTuple[2][3]) / 256 - 128))

    # (A-128) * 100/128
    elif dataTuple[0][1] in (list(range(0x0106,0x0109)) + [0x012D]):
        pidtuple[2][0] = int(round((dataTuple[0][3] * 128 / 100) + 128))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0]*-128) * 100/128))

    # Temperature
    # A - 40
    elif dataTuple[0][1] in [0x0105, 0x010F, 0x0146, 0x015C]:
        pidtuple[2][0] = int(round(dataTuple[0][3] + 40))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], dataTuple[2][0] - 40))

    # A * 3. Seems to be for high-pressure params. Like fuel pressure. Actually... only fuel pressure
    elif dataTuple[0][1] in [0x010A]:
        pidtuple[2][0] = int(round(dataTuple[0][3] // 3))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], dataTuple[2][0] * 3))

    # A     The value IS the value. Cool. Done
    elif dataTuple[0][1] in [0x010B, 0x010D, 0x0130, 0x0133]:
        pidtuple[2][0] = int(round(dataTuple[0][3]))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], dataTuple[2][0]))

    # (A *256 + B) / 4
    elif dataTuple[0][1] in [0x010C]:
        pidtuple[2][0] = int(round(dataTuple[0][3] * 4 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] * 4 % 256))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0]*256 + dataTuple[2][1]) / 4))

    # (A - 128) / 2
    elif dataTuple[0][1] in [0x010E]:
        pidtuple[2][0] = int(round(dataTuple[0][3] * 2 + 128))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] - 128) / 2))

    # ((A*256) + B) / 100
    elif dataTuple[0][1] in [0x0110]:
        pidtuple[2][0] = int(round(dataTuple[0][3] * 100 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] * 100 % 256))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] *256 + dataTuple[2][1]) / 100))

    # (A*256) + B
    elif dataTuple[0][1] in [0x0121, 0x0131, 0x014D, 0x014E]:
        pidtuple[2][0] = int(round(dataTuple[0][3] // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] % 256))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], dataTuple[2][0] *256 + dataTuple[2][1]))

    # ((A*256) + B) * 0.079
    elif dataTuple[0][1] in [0x0122]:
        pidtuple[2][0] = int(round(dataTuple[0][3] / 0.079 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] / 0.079 % 256))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] *256 + dataTuple[2][1])* 0.079))

    # ((A*256) + B) * 10
    elif dataTuple[0][1] in [0x0123]:
        pidtuple[2][0] = int(round(dataTuple[0][3] / 10 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] / 10 % 256))
        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] *256 + dataTuple[2][1])* 10))

    # ((A*256) + B) / 4 where A and B are signed
    elif dataTuple[0][1] in [0x0132]:

        pidtuple[2][0] = int(round(dataTuple[0][3] * 4 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] * 4 % 256))

        if pidtuple[2][0] < 0:
            pidtuple[2][0] += 256

        if pidtuple[2][1] < 0:
            pidtuple[2][1] += 256

        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] * 256 + dataTuple[2][1])))

    # ((A*256)+B) / 10 - 40
    elif dataTuple[0][1] in range(0x013C, 0x013F):
        pidtuple[2][0] = int(round((dataTuple[0][3] + 40) * 10 // 256))
        pidtuple[2][1] = int(round((dataTuple[0][3] + 40) * 10 % 256))

        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] *256 + dataTuple[2][1]) / 10 - 40))

    # ((A*256) + B) / 1000
    elif dataTuple[0][1] in range(0x013C, 0x013F):
        pidtuple[2][0] = int(round(dataTuple[0][3] * 1000 // 256))
        pidtuple[2][1] = int(round(dataTuple[0][3] * 1000 % 256))

        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] *256 + dataTuple[2][1]) / 1000))

    # ((A*256) + B) / 32768
    elif dataTuple[0][1] in range(0x013C, 0x013F):
        pidtuple[2][0] = int(round(dataTuple[0][3] * 32767 // 256))
        pidtuple[2][0] = int(round(dataTuple[0][3] * 32767 % 256))

        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], (dataTuple[2][0] *256 + dataTuple[2][1]) / 32768))

    # TODO: I got tired of writing these, and there are no more that My car supports. So I'm done

    # Final catch: make it a % across the byte-range
    # A*100/255
    else:
        pidtuple[2][0] = int(round(dataTuple[0][3] * 255 / 100))

        # vals.append((pid_name[dataTuple[0]], dataTuple[0], dataTuple[1], dataTuple[2][0] * 100/255))

    # Byte-range check
    for i in range(0, len(pidtuple[2])):
        if pidtuple[2][i] >= 256 or pidtuple[2][i] < 0:
            print("The passed in data is out of range: ", end='')
            print(dataTuple, end='')
            print(" -> ", end='')
            print(pidtuple)

    return pidtuple

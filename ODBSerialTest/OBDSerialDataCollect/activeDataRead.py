#!/usr/bin/env python3

import csv
import struct
import serial
import sys

from threading import Thread, Event
from threading import Lock

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


# My module that contains PID-processing utilities
import obd

debug = False


class StdInSerial:
    def read(self, *args):
        return sys.stdin.buffer.read(*args)

    def write(self, *args):
        return sys.stdout.buffer.write(*args)

    def close(self, *args):
        pass

if len(sys.argv) < 2:
    # print("Usage: %s <port> [baud]" % sys.argv[0])
    # exit(-1)
    dev = StdInSerial()
else:
    if len(sys.argv) > 2:
        baud = int(sys.argv[2])
    else:
        baud = 57600
    dev = serial.Serial(sys.argv[1], baud)



##########
#### Create the GUI
##########
class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.pids = [
                0x0104,
	        0x0105,
                0x010B,
                0x010C,
                0x010D,
                0x010E,
                0x010F,
                0x0110,
                0x0111,
                0x0128,
                0x012F,
                0x0133,
                0x0142,
                0x0146,
                0x0114
                ]

        self.moteChoice = QComboBox()
        self.moteChoice.addItem("All")
        self.msgInput = QLineEdit()
        self.sendButton = QPushButton("Send")

        # Table for storing values
        self.table = QTableWidget(len(self.pids), 3)
        # header = QStringList()
        # header.append("Name")
        # header.append("Value")
        self.table.setHorizontalHeaderLabels(["PID", "Name", "Value"])

        for i in range(0, self.table.rowCount()):
            for j in range(0, self.table.columnCount()):
                self.table.setCellWidget(i,j, QLabel())
            self.table.cellWidget(i,0).setText("%04x" % self.pids[i])
            self.table.cellWidget(i,1).setText(obd.pid_name[self.pids[i]])


        buttonLayout = QVBoxLayout()
        buttonLayout.addWidget(self.table)

        msgLayout = QHBoxLayout()
        msgLayout.addWidget(self.moteChoice)
        msgLayout.addWidget(self.msgInput)
        msgLayout.addWidget(self.sendButton)
        buttonLayout.addLayout(msgLayout)

        # self.submitButton.clicked.connect(self.submitContact)

        mainLayout = QGridLayout()
        # mainLayout.addWidget(nameLabel, 0, 0)
        mainLayout.addLayout(buttonLayout, 0, 1)

        self.setLayout(mainLayout)
        self.setWindowTitle("Hello Qt")

    def setValue(self, pidtuple):
        print("Setting value for ", pidtuple)
        # pid tuple => (name, pid, time, data)
        if pidtuple[1] in self.pids:
            self.table.cellWidget(self.pids.index(pidtuple[1]),2).setText(str(pidtuple[3]))

app = QApplication([])
screen = Form()
screen.show()

# This class, when created, will spawn a thread that continually tries to read from serial, and update the GUI
class ReceiveThread(Thread):
    def __init__(self, serial_device):
        #super(Thread, self).__init__()
        Thread.__init__(self)

        self.keepAlive = Event()
        self.serial_device = serial_device
        self.start()

    def run(self):

        while not self.keepAlive.isSet():
            # header = dev.read(7)
            expectedCC = dev.read(1)
            print("Read: ", expectedCC)
            while expectedCC != b'\xCC':
                expectedCC = dev.read(1)
                print("Read: ", expectedCC)
            typeindicator = dev.read(1)
            if (typeindicator[0] & 0x03) == 0x01:
                print("Timing!")
                continue
            elif (typeindicator[0] & 0x03) == 0x02:
                trash = dev.read(5)
                for i in range(0,5):
                    pidstr = dev.read(2)
                    if len(pidstr) == 0:
                        continue

                    print("pidstr: ", pidstr, "(%i)" % len(pidstr))
                    if pidstr is not None:
                        pid = struct.unpack("H", pidstr)[0]   # uint16_t
                        print("Pid: %i" % pid)
                        if pid not in obd.nbytes_by_pid:
                            bstr = dev.read(1)
                            while bstr[-1] != ord('\n'):
                                bstr += dev.read(1)
                                # print("Collected: ", bstr[-1])
                            print("Unknown PID: %s" % (pidstr + bstr))
                            continue

                        time = struct.unpack("I", dev.read(4))[0]  # uint32_t
                        print("Time: %i" % time)
                        data_length = obd.nbytes_by_pid[pid]
                        data = []
                        # Every single time, it's expected that all 4 bytes will be sent
                        for i in range(0,4):
                            data.append(struct.unpack("B", dev.read(1))[0])
                            data_length -= 1

                        print("Data: ", data)

                        if debug:
                            for datum in data:
                                print(datum, end=", ")
                            print("")

                        # Clear to endl
                        # bstr = dev.read(1)
                        # while bstr[-1] != ord('\n'):
                        #     bstr += dev.read(1)
                        #     # print("Collected: ", bstr[-1])
                        # if len(bstr) > 2:
                        #     print("Extra Trailing bytes: ", bstr)

                        # pid tuple => (name, pid, time, data)
                        manytuples = obd.processData((pid,time,data))
                        for pidtuple in manytuples:
                            screen.setValue(pidtuple)
                        # current_val[pid] = obd.processData((pid,time,data))

                        # Now, update the GUI
                        # print(current_val[pid])
                        # print(processData((pid,time,data)))

    def join(self, timeout=None):
        self.keepAlive.set()
        super().join(timeout)
        pass

# Creating this object causes it to spawn
receiver = ReceiveThread(dev)

appreturn = app.exec_()

# Wait for the GUI to exit, then stop the thread, and shut down nicely
receiver.join()
dev.close()

sys.exit(appreturn)

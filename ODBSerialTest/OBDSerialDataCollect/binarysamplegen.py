#!/usr/bin/env python3

import sys

# Simply generates some sample packets

# RPM = 2560 @ time 1
sys.stdout.buffer.write(  b'0x01' +
        b'0x0C' +
        b'0x00' +
        b'0x00' +
        b'0x00' +
        b'0x01' +
        b'0x0A' +
        b'0x00' +
        b'0x00' +
        b'0x00' +
        b'\r\n'
        )

# speed = 80km/s @ time 257
sys.stdout.buffer.write(  b'0x01' +
        b'0x0D' +
        b'0x00' +
        b'0x00' +
        b'0x01' +
        b'0x01' +
        b'0x50' +
        b'0x00' +
        b'0x00' +
        b'0x00' +
        b'\r\n'
        )

# Bank 1 Sensor 1 Voltage and Fuel Trim
sys.stdout.buffer.write(  b'0x01' +
        b'0x14' +
        b'0x00' +
        b'0x00' +
        b'0x02' +
        b'0x00' +
        b'0x10' +
        b'0x20' +
        b'0x00' +
        b'0x00' +
        b'\r\n'
        )

# O2S1 WR lambda
sys.stdout.buffer.write(  b'0x01' +
        b'0x14' +
        b'0x00' +
        b'0x00' +
        b'0x03' +
        b'0x00' +
        b'0x01' +
        b'0x00' +
        b'0x01' +
        b'0x00' +
        b'\r\n'
        )

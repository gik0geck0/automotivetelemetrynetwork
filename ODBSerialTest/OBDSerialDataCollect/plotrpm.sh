#!/bin/bash

gnuplot - <<PLOT
set terminal pngcairo
set output '$2'
set border 3
set xtics nomirror
set ytics nomirror
set title 'RPMs at 10Hz'
set xlabel 'time (ms)'
set ylabel 'RPM'
unset key
plot '$1' using 1:2
pause mouse
PLOT

#!/usr/bin/env python3

from sys import stdin, stdout, stderr
import obd
import struct
import fileinput
import binascii

# Sits on stdin, converting lines of lists of pid-tuples into bytes
for line in fileinput.input():
    # print("Line: %s" % line, file=stderr)
    if len(line) > 0:
        # first character is always [, and last is always ]
        # protect against malformed lines, though
        if line[0] == '[':
            # strip off the [ and ]
            line = line[1:-2]

            # We have a list of tuples
            tups = []
            while len(line) > 0 and line[0] == '(':
                # process the tuple's values, and place them into (name, pid, time, data)

                # Cut until the next ), since that's when the tuple will end. And yes, this assumes that there are no parens in the name
                onetuple = line[1:line.index(')')]

                # reduce the line to disclude that section
                line = line[len(onetuple)+2:]

                # split the tuple by comma. again, this assumes that the name does not have a comma
                tuplelist = onetuple.split(',')

                # fill in the tuple with types
                # also, the name is surrounded by quotes
                # (name, pid, time, value
                tups.append((tuplelist[0][1:-1], int(tuplelist[1]), int(tuplelist[2]), float(tuplelist[3])))

                # cut off the ', ' if it's there
                line = line[2:]

            pidtuple = obd.reverse(tups)
            # print(pidtuple)
            print(
                binascii.hexlify(
                struct.pack("H", pidtuple[0])
                + struct.pack("I", pidtuple[1])
                + struct.pack("B", pidtuple[2][0])
                + struct.pack("B", pidtuple[2][1])
                + struct.pack("B", pidtuple[2][2])
                + struct.pack("B", pidtuple[2][3])).decode('ascii'))

            # stdout.buffer.write(struct.pack("H", pidtuple[0]))
            # stdout.buffer.write(struct.pack("I", pidtuple[1]))
            # stdout.buffer.write(struct.pack("B", pidtuple[2][0]))
            # stdout.buffer.write(struct.pack("B", pidtuple[2][1]))
            # stdout.buffer.write(struct.pack("B", pidtuple[2][2]))
            # stdout.buffer.write(struct.pack("B", pidtuple[2][3]))
            # stdout.buffer.write(b'\r\n')

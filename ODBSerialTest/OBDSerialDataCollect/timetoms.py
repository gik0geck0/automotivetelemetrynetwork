#!/usr/bin/env python3

import sys

first = None
for line in sys.stdin:
    spl = line.split()
    time = int(spl[0])

    rpm = float(spl[1])

    if first is None:
        first = time

    print("%i %0.2f" % ((time-first)/1000, rpm))

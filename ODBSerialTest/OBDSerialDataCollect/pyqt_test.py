#!/usr/bin/env python3

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.nameLine = QLineEdit()
        self.submitButton = QPushButton("Submit")

        # Table for storing values
        self.table = QTableWidget(1, 2)
        # header = QStringList()
        # header.append("Name")
        # header.append("Value")
        self.table.setHorizontalHeaderLabels(["Name", "Value"])

        for i in range(0, self.table.rowCount()):
            for j in range(0, self.table.columnCount()):
                self.table.setCellWidget(i,j, QLabel())
        self.table.cellWidget(0,0).setText("NAME")

        buttonLayout1 = QVBoxLayout()
        buttonLayout1.addWidget(self.table)
        buttonLayout1.addWidget(self.nameLine)
        buttonLayout1.addWidget(self.submitButton)

        self.submitButton.clicked.connect(self.submitContact)

        mainLayout = QGridLayout()
        # mainLayout.addWidget(nameLabel, 0, 0)
        mainLayout.addLayout(buttonLayout1, 0, 1)

        self.setLayout(mainLayout)
        self.setWindowTitle("Hello Qt")

    def submitContact(self):
        name = self.nameLine.text()
        self.table.cellWidget(0,1).setText(name)

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    screen = Form()
    screen.show()

    sys.exit(app.exec_())

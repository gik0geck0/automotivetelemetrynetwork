#include <stdio.h>
#include <stdint.h>

int main() {
    uint16_t command = 0x14;
    char cmdstr[8];
    sprintf(cmdstr, "%04x", command);

    printf(cmdstr);
    printf("\n");

    printf("%i\n%i\n", sizeof(uint32_t), sizeof(char));
}

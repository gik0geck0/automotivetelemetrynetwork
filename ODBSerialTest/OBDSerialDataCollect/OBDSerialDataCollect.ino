// This file contains code that will collect a set of PIDs, then immediately
// send it over XBee.
#include <SoftwareSerial.h>

// Constructor is (receive, send, invers_logical=false
// SoftwareSerial xbee(3,4);
// calling the constructor here causes a linker error.
// To get around that, the xbee is initialized in the setup scope,
// and placed here
SoftwareSerial* obd;

#define DEBUG 0

// 20 ms is about the fastest that works.
// For comparison, most commands take 20ms to get their response
#define OBDDELAY 20

// Struct that defines our data
// time: number of micros since either the car was started or the mote was started. This needs work
// pid: 2-byte ID for the OBD-II pid. The hex value of this int is exactly the hex for the pid defined at http://en.wikipedia.org/wiki/OBD-II_PIDs
//
// length: number of bytes used by the value.
// value: variable-length (defined by lenght) raw bytes from the OBD-II adapter.
//      As defined at the OBD-II_PIDs page, complex values are calculated from multiple bytes
//      1 byte: A  00 00 00
//      2 byte: A  B  00 00
//      4 byte: A  B  C  D
//
//      This is synonymous with the order that network bytes use, and the order that OBD-II produces them
// Note: the order of these values makes it really easy to write the struct to a buffer, sending only the proper number of bytes in value
// the passed in buffer must use bytes, since sizeof and length both measure in bytes
// write((uint8_t*) data, sizeof(uint16_t) + sizeof(uint32_t) + length)
// Note: this will only work for ONE of the data structs... if using more, they must be looped over, and accessed individually from the start
struct data {
    uint16_t pid;
    uint32_t time;
    uint8_t value[4];
    // uint8_t length;
};

// List of commands to query
// #define NCMD 1
// uint16_t commands[NCMD];

void send_save_forward(uint16_t command);
void send_recv(char* command);
struct data send_recv(uint16_t command);
void getResponse(int lines);

//This is a character buffer that will store the data from the serial port
char rxData[100];
uint8_t rxIndex=0;

// Data buffer with untransmitted values
// 100 * 2 * 16 = 3,200 bytes = 3.2k
#define BUFFERSIZE 100
struct data dataBuffer[BUFFERSIZE];
int bufferRead = 0;
int bufferWrite = 0;

void setup() {
	// Setup the commands to be sent
	
    Serial.begin(57600);

    // Made with new because for whatever reason, there's a linker error when its statically allocated
    obd = new SoftwareSerial(2,3);

    // 9600 and 19200 were proven to work
    (*obd).begin(9600);
	// (*obd).begin(19200);

    //Wait for a little while before sending the reset command to the OBD-II-UART
    delay(2000);

    //Reset the OBD-II-UART
    (*obd).println("ATZ");
	getResponse(1);
	getResponse(1);
	delay(100);
    (*obd).flush();
	
	(*obd).println("ATE0");
	
	getResponse(1);
	if (DEBUG) {
		Serial.print("Rx Data: ");
		Serial.println(rxData);
	}
	
	getResponse(1);
	if (DEBUG) {
		Serial.print("Rx Data: ");
		Serial.println(rxData);
	}
	
	getResponse(1);
	if (DEBUG) {
		Serial.print("Rx Data: ");
		Serial.println(rxData);
	}
	
    delay(100);
    (*obd).flush();

    //Wait for a bit before starting to send commands after the reset.
    delay(2000);

    //Delete any data that may be in the serial port before we begin.
    (*obd).flush();
}

void loop() {   
    //Delete any data that may be in the serial port before we begin.
    (*obd).flush();

	/*
    send_save_forward(0x0104);
	send_save_forward(0x0105);
    send_save_forward(0x010B);
	*/
    send_save_forward(0x010C);
	/*
    send_save_forward(0x010D);
	send_save_forward(0x010E);
    send_save_forward(0x010F);
    send_save_forward(0x0110);
    send_save_forward(0x0111);
	send_save_forward(0x0128);
	send_save_forward(0x012F);
    send_save_forward(0x0133);
	send_save_forward(0x0142);
    send_save_forward(0x0146);
    send_save_forward(0x0114);
	*/
	
	delay(50);
}

// Asks for a pid, saves it into the buffer, then immediately sends it over Xbee
void send_save_forward(uint16_t command) {
    dataBuffer[bufferWrite] = send_recv(command);

    // Only move to the next buffer slot AND send the data ONLY if the query got a response
    if (dataBuffer[bufferWrite].pid != 0) {
        Serial.write(((uint8_t*) &dataBuffer[bufferWrite]), sizeof(struct data));
        Serial.write('\r');
        Serial.write('\n');

        bufferWrite = (bufferWrite + 1) % BUFFERSIZE;
    }
}

struct data send_recv(uint16_t command) {
    // Convert the int-pid into its corresponding hex-pid
	char cmdstr[8];
    sprintf(cmdstr, "%04x", command);
	
	if (DEBUG) {
		Serial.print("CMD: ");
		Serial.println(cmdstr);
	}
	
	// start the timer. For now, we're going to time how long it takes to query and get a response
	long starttime = micros();
	
	// Clear the receive buffer first
	memset(rxData, 0, 100);
    send_recv(cmdstr);
	
    // Create the storage container
    struct data value;

    // Zero out the value first
    memset(&value, 0, sizeof(struct data));
		
	// If we didn't get a response, notify the caller
	if ( strlen(rxData) == 0
		|| (strlen(rxData) == 1 && rxData[0] == '\r')
		|| strcmp(rxData, "NO DATA\r") == 0
		|| strcmp(rxData, "STOPPED\r") == 0
		|| strcmp(rxData, "SCANNING...\r") == 0) {
		value.pid = NULL;
		return value;
	}

    // PID
    value.pid = command;
	
	/*
	Serial.print("Cmd (");
	Serial.print(command);
	Serial.print(")");
	Serial.print(cmdstr);
	Serial.print(" - ");
	Serial.write((uint8_t*) &value.pid, 2);
	Serial.print("-");
	Serial.println("");
	*/
	
    // mote-time in micros
    // This is the best timestamp we can give for now
    // TODO: Include vehicle time
    // Create a compound time using car_seconds + mote micros
    // This will require finding last time query, micros last time query,
    // and then use the last car secs + micros diff from that last time query to hopefully accurately state
    // the partial-time
    // Trouble: synchronizing 0s on car = 0micros on mote, since the seconds on car may change in mid-query
	long endtime = micros();
    value.time = endtime;

    // Set each byte that exists
    int length = strlen(rxData)/3 - 2;
	
	if (DEBUG) {
		Serial.print("Rx Data: ");
		Serial.println(rxData);
	}
	
	// Serial.print("Data Bytes [");
    for (int i=0; i < length; ++i) {
		/*
		Serial.print("(");
		Serial.print(i);
		Serial.print(",");
		*/
        value.value[i] = strtol(&rxData[6 + i*3],0,16);
		/*
		Serial.print(value.value[i]);
		Serial.print("), ");
		*/
    }
	// Serial.println("]");

    return value;
}

void send_recv(char* command) {
    (*obd).println(command);
	// (*obd).write('\r');
	// Serial.print("CMD: ");
	// Serial.println(command);
    int lines = 1;
    if (strcmp(command, "0100"))
        lines = 2;
    // getResponse(lines); // echo
    getResponse(lines); // data

    // Serial.print("Receive: ");
    // Serial.println(rxData);
    delay(OBDDELAY);   // give OBD a break
    (*obd).flush(); // clear out old data
}

//The getResponse function collects incoming data from the UART into the rxData buffer
// and only exits when a carriage return character is seen. Once the carriage return
// string is detected, the rxData buffer is null terminated (so we can treat it as a string)
// and the rxData index is reset to 0 so that the next string can be copied.
void getResponse(int lines) {
    char inChar=0;
    //Keep reading characters until we get a carriage return
    while(inChar != '\r' && lines > 0){
        //If a character comes in on the serial port, we need to act on it.
        if((*obd).available() > 0){
            //Start by checking if we've received the end of message character ('\r').
            if((*obd).peek() == '\r'){
                lines--;
                //Clear the Serial buffer
                inChar = (*obd).read();
                // Add the newline to the buffer. Have too many characters is fine
                rxData[rxIndex++]=inChar;
            }
            // If we didn't get the end of message character, just add the new character to the string.
            else{
                //Get the new character from the Serial port.
                inChar = (*obd).read();
                //Add the new character to the string, and increment the index variable.
                rxData[rxIndex++]=inChar;
            }
        }
    }

    //Put the end of string character on our data string
    rxData[rxIndex]='\0';
    //Reset the buffer index so that the next character goes back at the beginning of the string.
    rxIndex=0;
}

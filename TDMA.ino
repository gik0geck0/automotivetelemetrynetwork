//IMPORTANT!!!!!!
//to use this, you should download x-ctu to configure your dongles, from the default
//settings, change the following values as stated
//DL (Destination Address Low) should be set to FFFF
//MM (MAC mode) should be set to 1
//RR (XBee Retries) should be set to 0
//RN (Randome Delay Slots) should be set to 0
//CA (CCA Threshold) should be set to 24 (0 would be better, but it won't let me go that high)
//RO (Packetization Timeout) should be set to 3 (this defines idle time on serial line before sending, 1 should be workable, though I haven't tried, 0 would be ideal, though I fear it would waste time packetizing and actually be slower than a non 0 value)
//this is currently coded using a hacked MsTimer2 which assumes a 16ms timer resolution in the set call.  If you're using a standard MsTimer2, change the values accordingly for a 1ms resolution.
#include "MsTimer2.h"
#include "types.h"

#define MY_ID 0
#define NUM_IDS 2
#define DEBUG 1
#define TEST_DATA 1

//#if MY_ID == 0
//byte data[] = {'a','b','c','d','e','f','g','h','i','j','a','b','c','d','e','f','g','h','i','j','a','b','c','d','e','f','g','h','i','j','a','b','c','d','e','f','g','h','i','j','a','b','c','d','e','f','g','h','i','j','a','b','c','d','e','f','g','h','i','j'};
//#else
//byte data[] = {'1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9','0'};
//#endif


#define MSG_TYPE_TIMING 1
#define MSG_TYPE_DATA 2

poolItem_t *activeListHead, *activeListTail, *idleListHead;
#define POOL_SIZE 10
poolItem_t items[POOL_SIZE];
unsigned long currMsgId, lastSeenMsgId[NUM_IDS], time1, time2;
unsigned char currentId;
boolean alreadySent, sendData;
int myId;

void printStats() {
  #ifdef DEBUG
    Serial.println("activeHead");
    Serial.println((int)activeListHead);
    Serial.println("activeTail");
    Serial.println((int)activeListTail);
    Serial.println("idleHead");
    Serial.println((int)idleListHead);
    for(int i = 0; i < POOL_SIZE; i++) {
      Serial.println("next");
      Serial.println((int)&items[i]);
      Serial.println((int)items[i].next);
      Serial.println((int)items[i].prev);
    }
  #endif
}

void setupLists() {
  activeListHead = activeListTail = NULL;
  for(int i = 0; i < POOL_SIZE; i++) {
    if(!i) {
      items[i].prev = NULL;
    } else {
      items[i].prev = &items[i-1];
    }

    if(i == POOL_SIZE - 1) {
      items[i].next = NULL;
    } else {
      items[i].next = &items[i+1];
    }
  }
  idleListHead = &items[0];
  
  printStats();
}

void flash() {
  if(currentId % NUM_IDS == myId) {
    sendData = true;
  }
  alreadySent = true;
  currentId++;
}

void handleTimeMsg(header_t msg) {
  MsTimer2::start();
  currentId = msg.senderId;
  if(!alreadySent) {
    flash();
  } else {
    //if we reset our currentId, we need to increment always
    currentId++;
  }
  alreadySent = false;
}

poolItem_t* getPoolItem() {
  //get our item that we're going to write in to
  poolItem_t *activeItem;
  if(idleListHead) {
    activeItem = idleListHead;
    idleListHead = activeItem->next;
    if(idleListHead) {
      idleListHead->prev = NULL;
    }
  } else {
    activeItem = activeListTail;
    (activeItem->prev)->next = NULL;
    activeListTail = activeItem->prev;
  }
  return activeItem;
}

void removeItemFromActive(poolItem_t * item) {
  if(item->prev) {
    item->prev->next = item->next;
  } else {
    activeListHead = item->next;
  }
  if(item->next) {
    item->next->prev = item->prev;
  } else {
    activeListTail = item->prev;
  }
  item->next = idleListHead;
  if(idleListHead) {
    idleListHead->prev = item;
  }
  idleListHead = item;
  idleListHead->prev = NULL;
}

void removeQueuedItem(poolItem_t *item) {
  poolItem_t *i = activeListHead;
  while(i) {
    if(i->data.msgId == item->data.msgId) {
      removeItemFromActive(i);
      break;
    }
    i = i->next;
  }
  //re-insert item into idle list
  item->next = idleListHead;
  if(idleListHead) {
    idleListHead->prev = item;
  }
  idleListHead = item;
  idleListHead->prev = NULL;
}

byte isMessageQueued(poolItem_t* item) {
  byte retVal = 0;
  poolItem_t *i = activeListHead;
  while(i) {
    if(i->data.msgId == item->data.msgId) {
      retVal = 1;
      break;
    }
    i = i->next;
  }
  return retVal;
}

void addIfValid(poolItem_t *item) {
  //don't add it if it's less than what we've already seen
  unsigned long msgId = item->data.msgId;
  unsigned char id = item->data.header.senderId;

  if(msgId > lastSeenMsgId[id] || (id == myId && msgId == lastSeenMsgId[id])) {
    item->rebroadcastCount = 0;
    if(msgId > lastSeenMsgId[id]) {
      lastSeenMsgId[id] = msgId;
    }
    //add to the head
    if(activeListHead) {
      activeListHead->prev = item;
      item->next = activeListHead;
      item->prev = NULL;
      activeListHead = item;
    } else {
      item->next = NULL;
      item->prev = NULL;
      activeListHead = item;
      activeListTail = item;
    }
  } else {
    //put the item back into the idle list
    item->prev = NULL;
    item->next = idleListHead;
    if(idleListHead) {
      idleListHead->prev = item;
    }
    idleListHead = item;
  }
}

void putItemBackInIdle(poolItem_t *item) {
  item->next = idleListHead;
  if(idleListHead) {
    idleListHead->prev = item;
  }
  item->prev = NULL;
  idleListHead = item;
}

void processData(header_t header) {
  poolItem_t *activeItem = getPoolItem();
  //a bit of casting magic to make a structure turn into a byte buffer
  //this will allow the serial.read to fill out the structure fully auto-magically
  char *buf = ((char*)&(activeItem->data));
  //a bit of casting magic to allow me to fill out the struct buf is pointing to
  header_t* hdr = ((header_t*)&buf[0]);
  (*hdr) = header;
  byte numBytes = Serial.readBytes(&(buf[sizeof(header_t)]), sizeof(packetData_t) - sizeof(header_t));
  
  //check to make sure we read the correct number of bytes
  if(numBytes != sizeof(packetData_t) - sizeof(header_t)) {
    putItemBackInIdle(activeItem);
    return;
  }
  
  //is this message meant for us?
  if(activeItem->data.dest == myId) {
    //retransmit with a "don't rebroadcast" to stop flooding
    activeItem->data.header.retransmit = 0;
    addIfValid(activeItem);
    //TODO: do something with the data
    Serial.print("GotsIt\n");
  } else if(!(activeItem->data.header.retransmit)) {
  //if it's a message indicated not to replicate
    removeQueuedItem(activeItem);
  } else if(isMessageQueued(activeItem)) {
    //if we've heard a message we had queued to re-broadcast, remove it
    removeQueuedItem(activeItem);
  } else {
    addIfValid(activeItem);
  }
}

void sendMsgs() {
  static boolean output = HIGH;
  digitalWrite(13, output);
  output = !output;
  //send a time sync message first
  timeSync_t time;
  poolItem_t *sendItem, *nextToSend;
  time.header.checkByte = 0xCC;
  time.header.msgType = 1; //MSG_TYPE_TIMING;
  time.header.retransmit = 0;
  time.header.senderId = myId;
  Serial.write((unsigned char*)(&time), sizeof(time));
  Serial.flush();
  #ifdef DEBUG
  Serial.println(time.header.senderId);
  Serial.println(time.header.retransmit);
  Serial.println(time.header.msgType);
  #endif
  //now send any data I have queued
  sendItem = activeListTail;
  while(sendItem) {
    #ifdef DEBUG
    Serial.println("sending item");
    Serial.println((int)sendItem);
    #endif
    sendItem->data.header.checkByte = 0xCC;
    Serial.write((unsigned char*)&sendItem->data, sizeof(sendItem->data));
    Serial.flush();
    nextToSend = sendItem->prev;
    sendItem->rebroadcastCount++;
    if(sendItem->rebroadcastCount > 5 || !sendItem->data.header.retransmit) {
      #ifdef DEBUG
      Serial.println("removedItem");
      #endif
      removeItemFromActive(sendItem);
    }
    sendItem = nextToSend;
    printStats();
  }
}

#ifdef TEST_DATA
//example send message routine
void pushData() {
  Serial.println("queuedData");
  poolItem_t *activeItem = getPoolItem();
  activeItem->data.header.msgType = MSG_TYPE_DATA;
  activeItem->data.header.retransmit = 1;
  activeItem->data.header.senderId = myId;
  activeItem->data.msgId = ++lastSeenMsgId[myId];
  activeItem->data.dest = (myId + 3) % NUM_IDS; //make up a node to send it to, just make sure you don't have 3 nodes in this case, though it technically should still work.
  addIfValid(activeItem);
  printStats();
}
#endif

void setup() {
  // put your setup code here, to run once:
  Serial.begin(57600);
  Serial.setTimeout(200); //only wait 20 milliseconds for data to arrive, don't want to block here too long
  pinMode(13, OUTPUT);
  MsTimer2::set(60, flash); //modified to 16ms resolution, so 6*16 = 96ms
  setupLists();
  myId = MY_ID;
  currMsgId = 0;
  alreadySent = sendData = false;
  for(int i = 0; i < NUM_IDS; i++) {
    lastSeenMsgId[i] = 1;
  }
  //this will allow node 0 to start and then start the others
  //this could be used as a way to keep other nodes idle until
  //ready to go, however, once they're all going, they're all
  //going to go regardless
  if(myId == 0) {
    MsTimer2::start();
  }
}

int count;
void loop() {
  header_t header;
  char* hdr;
  if(sendData) {
    sendMsgs();
    sendData = false;
  }
  // put your main code here, to run repeatedly:
  if(Serial.available()) {
    //yey for more casting magic
    unsigned char* hdr = ((unsigned char*)&header);
    
    *hdr = Serial.read();
    //all valid messages are pre-appended with 0xCC to make usre it's valid as that's not a terribly common value to see at random
    if(*hdr == 0xCC) {
      //move to the next byte of the header
      //Serial.println("got here");
      hdr++;
      #ifdef TEST_DATA
      count++;
      #endif

      #ifdef DEBUG
      if(myId) {
        Serial.print("node1 got ");
      } else {
        Serial.print("node0 got ");
      }
      #endif
      //have to use readBytes because the second byte might not actually be here yet and read returns immediately
      Serial.readBytes((char*)hdr, sizeof(char));
      #ifdef DEBUG
      Serial.println((byte)*hdr);
      #endif
      switch(header.msgType) {
        case MSG_TYPE_TIMING:
          handleTimeMsg(header);
          break;
        case MSG_TYPE_DATA:
          processData(header);
          break;
        default:
          //clear out any invalid data and toss it
          while(Serial.available()) {
            Serial.read();
          }
          break;
      }
    } else {
      while(Serial.available()) {
        Serial.read();
      }
    }
    #ifdef TEST_DATA
    //randomly create data for test purposes, not initialized on purpose for hopefully spreading out when each sends
    if(count % 20 == 0) {
      pushData();
      count++;
    }
    //count++;
    #endif
  }
  *hdr = 0;
}

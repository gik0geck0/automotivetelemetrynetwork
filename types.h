#ifndef __TYPES_H__
#define __TYPES_H__

//this file is actually a requirement, it turns out the arduino compiler sucks so bad that it won't
//recognize a structure defined in the same file in which it's used
typedef struct {
  unsigned char checkByte;
  unsigned char msgType : 2;
  unsigned char retransmit : 1;
  unsigned char senderId : 5; //realistically won't use this many users, but maybe if needed still a bit here and throw it at msgType if needed
} header_t;

typedef struct {
  header_t header;
  //no actual data needed beyond what's in the header
} timeSync_t;

typedef struct {
  header_t header;
  unsigned long msgId; //being used, hopefully as a window to know if a message has been acked or not, not 100% on how to use this, just know that I want to use it to stop flooding
  unsigned char dest;
  //Add whatever extra data needs to go here
  //was thinking static fields so we'd have a fixed length
} packetData_t;

struct BUFFER_POOL_ITEM;

typedef struct BUFFER_POOL_ITEM {
  struct BUFFER_POOL_ITEM *next;
  struct BUFFER_POOL_ITEM *prev;
  unsigned char rebroadcastCount;
  packetData_t data;
} poolItem_t;

#endif //__TYPES_H__
